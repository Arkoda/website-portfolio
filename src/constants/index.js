import { facebook, instagram, linkedin, twitter, send, shield, star, breadcrumb, bluepacman, weatherhub  } from "../assets";

export const navLinks = [
  {
    id: "home",
    title: "Home",
  },
  {
    id: "education",
    title: "Education",
  },
  {
    id: "skills",
    title: "Skills",
  },
  {
    id: "project",
    title: "Projects",
  },
  //{
    //id: "login",
    //title: "Login",
  //},
  {
    id: "docs",
    title: "Documents",
  },
];

export const stats = [
  {
    id: "stats-1",
    title: "BWD Bern",
    value: "2021-2024",
  },
  {
    id: "stats-2",
    title: "RIK Plus",
    value: "2019-2021",
  },
  {
    id: "stats-3",
    title: "Italian School",
    value: "2009-2019",
  },
];

export const footerLinks = [
  {
    title: "Contacts",
    links: [
      {
        name: "Email",
        href: "mailto:giuseppegreco.work@hotmail.com",
      },
      {
        name: "Telephone Number",
        href: "tel:+41783389090", // Use "tel:" for phone numbers
      },
      {
        name: "Address",
        url: "Giacommettistrasse 25, 3006 Bern",
      },
      {
        name: "Impressum", // Add the new contact item
        url: "/impressum", // Replace with the actual URL or contact details
      },
    ],
  },
];

export const socialMedia = [
  {
    id: "social-media-4",
    icon: linkedin,
    link: "https://www.linkedin.com/in/giuseppe-greco-12b81828b/",
  },
];

export const skills = [
  { skill: 'HTML und CSS', percentage: 85 },
  { skill: 'C#', percentage: 20 },
  { skill: 'JavaScript', percentage: 70 },
  { skill: 'Java', percentage: 40 },
  { skill: 'SQL', percentage: 70 },
];

export const pdfFiles = [
  { url: '/CV_Giuseppe_Greco.pdf', type: 'resume',  name: 'Resume' },
  { url: '/GIBB_Zeugnis.pdf', type: 'gibb',  name: 'GIBB Certificates' },
  { url: '/BWD_Zeugnis.pdf', type: 'bwd',  name: 'BWD Certifcates' },
  { url: '/106.pdf', type: 'uk', name: 'ük 106' },
  { url: '/187.pdf', type: 'uk', name: 'ük 187' },
  { url: '/210.pdf', type: 'uk', name: 'ük 210' },
  { url: '/294.pdf', type: 'uk', name: 'ük 294' },
  { url: '/295.pdf', type: 'uk', name: 'ük 295' },
  { url: '/335.pdf', type: 'uk', name: 'ük 335' },
  // Add more PDF files here
];

export const PROJECTS = [
  {
    title: 'Breadcrumb',
    description: 'Breadcrumb is an innovative Restaurant Management system designed to enhance the dining experience. It enables seamless interaction between clients and NFC tags placed on each table, ensuring top-notch service and quality.',
    pdfUrl: './IMS_Projekt-Abstract_BREADCRUMB.pdf',
    imageUrl: breadcrumb,
  },
  {
    title: 'Blue Pac-Man',
    description: 'A vibrant twist on the classic Pac-Man game, featuring our beloved hero in a brilliant shade of blue. Navigate mazes, eat dots, and dodge ghosts in this exciting JavaScript recreation.',
    pdfUrl: './COMING SOON.pdf',
    imageUrl: bluepacman,
  },
  {
    title: 'WeatherHub',
    description: 'WeatherHub is a one-week Android weather app. It uses two APIs to provide real-time weather data for any location worldwide. The app also offers a user-friendly search bar for easy location lookup.',
    pdfUrl: './COMING SOON.pdf',
    imageUrl: weatherhub,
  },
  // Add more projects as needed
];