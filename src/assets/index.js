import Logo from "./Logo.png";
import quotes from "./quotes.svg";
import robot from "./robot.png";
import send from "./Send.svg";
import shield from "./Shield.svg";
import star from "./Star.svg";
import menu from "./menu.svg";
import close from "./close.svg";
import google from "./google.svg";
import apple from "./apple.svg";
import arrowUp from "./arrow-up.svg";
import discount from "./Discount.svg";
import facebook from "./facebook.svg";
import instagram from "./instagram.svg";
import linkedin from "./linkedin.svg";
import twitter from "./twitter.svg";
import longLogo from "./longLogo.png";
import ukplaceholder from "./ukplaceholder.png";
import cvplaceholder from "./cvplaceholder.png";
import gibbplaceholder from "./gibbplaceholder.png";
import bwdplaceholder from "./bwdplaceholder.png";
import breadcrumb from "./breadcrumb.png";
import bluepacman from "./bluepacman.png";
import weatherhub from "./weatherhub.png";

export {
  Logo,
  quotes,
  robot,
  send,
  shield,
  star,
  menu,
  close,
  google,
  apple,
  arrowUp,
  discount,
  facebook,
  instagram,
  linkedin,
  twitter,
  longLogo,
  ukplaceholder,
  cvplaceholder,
  gibbplaceholder,
  bwdplaceholder,
  breadcrumb,
  bluepacman,
  weatherhub,
};
