import React, { useState } from "react";
import styles from "../style";
import { longLogo } from "../assets";
import { footerLinks, socialMedia } from "../constants";
import AddressPopup from "./AddressPopup"; // Import the AddressPopup component'

const impressumContent = `
  <div>
    <p><strong>Responsible entity:</strong></p>
    <p>Giuseppe Greco</p>
    <p>Giacomettistrasse 25</p>
    <p>3006 Bern</p>
    <p>Switzerland</p>
    <p>Email: giuseppegreco.work@hotmail.com</p>
  </div>

  <div>
    <p><strong>Disclaimer</strong></p>
    <p>The author assumes no liability for the correctness, accuracy, timeliness, reliability and completeness of the information.</p>
    <p>Liability claims against the author for material or immaterial damage resulting from access to, use or non-use of the published information, from misuse of the connection or from technical malfunctions are excluded.</p>
    <p>All offers are non-binding. The author expressly reserves the right to change, add to, or delete parts of the pages or the entire offer without prior notice, or to temporarily or permanently cease publication.</p>
  </div>

  <div>
    <p><strong>Disclaimer for content and links</strong></p>
    <p>References and links to third party websites are outside our area of responsibility. It rejected any responsibility for such websites. Access to and use of such websites is at the user's own risk.</p>
  </div>

  <div>
    <p><strong>Copyright declaration</strong></p>
    <p>The copyrights and all other rights to content, images, photos or other files on this website belong exclusively to Giuseppe Greco or the specifically named rights holders. The written consent of the copyright holder must be obtained in advance for the reproduction of any elements.</p>
  </div>
`;

const Footer = () => {
  const [isAddressPopupOpen, setAddressPopupOpen] = useState(false);
  const [isImpressumPopupOpen, setImpressumPopupOpen] = useState(false);

  const openAddressPopup = () => {
    setAddressPopupOpen(true);
  };

  const closeAddressPopup = () => {
    setAddressPopupOpen(false);
  };

  const openImpressumPopup = () => {
    setImpressumPopupOpen(true);
  };

  const closeImpressumPopup = () => {
    setImpressumPopupOpen(false);
  };

  return (
    <section className={`${styles.flexCenter} ${styles.paddingY} flex-col`}>
      <div className={`${styles.flexStart} md:flex-row flex-col mb-8 w-full`}>
        <div className="flex-[1] flex flex-col justify-start mr-10">
          <img
            src={longLogo}
            alt="hoobank"
            className="w-[266px] h-[72.14px] object-contain"
          />
          <p className={`${styles.paragraph} mt-4 max-w-[312px]`}>
            For any additional information, contact me at one of the following contacts.
          </p>
        </div>

        <div className="flex-[1.5] w-full flex flex-row justify-between flex-wrap md:mt-0 mt-10">
          {footerLinks.map((footerlink) => (
            <div key={footerlink.title} className={`flex flex-col ss:my-0 my-4 min-w-[150px]`}>
              <h4 className="font-poppins font-medium text-[18px] leading-[27px] text-white">
                {footerlink.title}
              </h4>
              <ul className="list-none mt-4">
                {footerlink.links.map((link, index) => (
                  <li
                    key={link.name}
                    className={`font-poppins font-normal text-[16px] leading-[24px] text-dimWhite hover:text-secondary cursor-pointer ${
                      index !== footerlink.links.length - 1 ? "mb-4" : "mb-0"
                    }`}
                  >
                    {link.name === "Address" ? (
                      <span onClick={openAddressPopup}>{link.name}</span>
                    ) : link.name === "Impressum" ? (
                      <span onClick={openImpressumPopup}>{link.name}</span>
                    ) : link.href ? (
                      <a href={link.href}>{link.name}</a>
                    ) : (
                      <span>{link.name}</span>
                    )}
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      </div>

      <div className="w-full flex justify-between items-center md:flex-row flex-col pt-6 border-t-[1px] border-t-[#3F3E45]">
        <p className="font-poppins font-normal text-center text-[18px] leading-[27px] text-white">
          Copyright Ⓒ 2023 Giuseppe Greco. All Rights Reserved.
        </p>

        <div className="flex flex-row md:mt-0 mt-6">
          {socialMedia.map((social, index) => (
            <img
              key={social.id}
              src={social.icon}
              alt={social.id}
              className={`w-[21px] h-[21px] object-contain cursor-pointer ${
                index !== socialMedia.length - 1 ? "mr-6" : "mr-0"
              }`}
              onClick={() => window.open(social.link)}
            />
          ))}
        </div>
      </div>

      {/* Render the AddressPopup and ImpressumPopup components */}
      <AddressPopup isOpen={isAddressPopupOpen} onClose={closeAddressPopup} address={footerLinks[0].links[2].url} />
      <AddressPopup isOpen={isImpressumPopupOpen} onClose={closeImpressumPopup} address={impressumContent} />
    </section>
  );
};

export default Footer;
