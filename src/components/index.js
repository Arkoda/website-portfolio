import Navbar from "./Navbar";
import Hero from "./Hero"
import Footer from "./Footer";
import Stats from "./Stats";
import SkillBar from "./Skillbar";
import PdfGallery from "./PdfGallery";
import Projects from "./Projects";
import AddressPopup from "./AddressPopup";

export {
    Navbar,
    Hero,
    Stats,
    Footer,
    SkillBar,
    PdfGallery,
    Projects,
    AddressPopup,
};