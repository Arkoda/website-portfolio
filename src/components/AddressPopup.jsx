import React from "react";
import ReactDOM from "react-dom";
import styles from "../style"; // Import your CSS styles

const AddressPopup = ({ isOpen, onClose, address }) => {
  if (!isOpen) return null;

  const popupStyles = {
    maxHeight: "80vh", // Set a maximum height for the popup
    overflowY: "auto", // Enable vertical scrolling if needed
  };

  return ReactDOM.createPortal(
    <div className={`popup ${styles.modalOverlay}`}>
      <div className={styles.modal} style={popupStyles}>
        <div>
          <button
            className={`absolute top-5 right-5 px-2 py-1 font-bold text-blue-500 bg-white rounded-full ${styles.modalCloseButton}`}
            onClick={onClose}
          >
            ✖
          </button>
        </div>
        <div className={styles.modalContent}>
          <div className="mt-12 mb-2">
            <div dangerouslySetInnerHTML={{ __html: address }} />
          </div>
        </div>
      </div>
    </div>,
    document.body
  );
};

export default AddressPopup;
