import React from 'react';
import { PROJECTS } from '../constants';

function Projects() {
  return (
    <div id="project" className="flex flex-col">
      <h1 className="text-3xl md:text-4xl lg:text-5xl font-semibold text-center text-gradient mb-2 leading-normal">
        Projects
      </h1>
      <p className="text-xl md:text-2xl lg:text-xl font-medium text-center text-white text-gradient mb-8">
        Click on the projects logo to visualize the project document.
      </p>
      {PROJECTS.map((project, index) => (
        <div
          key={index}
          className={`${
            index % 2 === 0
              ? 'max-w-3xl w-full p-8 box-shadow mb-8 text-gradient text-left flex'
              : 'ml-auto max-w-3xl w-full p-8 box-shadow mb-8 text-gradient text-right flex'
          }`}
          style={{ height: 'auto' }}
        >
          {index % 2 === 0 ? (
            <>
              <div className="rounded-full bg-white p-3 cursor-pointer w-16 h-16" style={{ flex: 'none' }}>
                <a href={project.pdfUrl} target="_blank" rel="noopener noreferrer">
                  <img src={project.imageUrl} alt={`${project.title} Preview`} className="w-full h-full object-cover" />
                </a>
              </div>
              <div className="ml-4">
                <h2 className="text-3xl md:text-4xl lg:text-5xl font-semibold text-gradient">{project.title}</h2>
                <p className="text-white text-lg mt-3 mb-4">{project.description}</p>
              </div>
            </>
          ) : (
            <>
              <div className="mr-4 text-right">
                <h2 className="text-3xl md:text-4xl lg:text-5xl font-semibold text-gradient">{project.title}</h2>
                <p className="text-white text-lg mt-3 mb-4">{project.description}</p>
              </div>
              <div className="rounded-full bg-white p-3 cursor-pointer w-16 h-16" style={{ flex: 'none' }}>
                <a href={project.pdfUrl} target="_blank" rel="noopener noreferrer">
                  <img src={project.imageUrl} alt={`${project.title} Preview`} className="w-full h-full object-cover" />
                </a>
              </div>
            </>
          )}
        </div>
      ))}
    </div>
  );
}

export default Projects;
