import React from 'react';
import { skills } from '../constants';

const SkillBar = () => {
  return (
    <div id="skills" className="container mx-auto p-4">
      <h1 className="text-2xl font-semibold mb-4 text-gradient text-white">Skills</h1>
      {skills.map((skill, index) => (
        <div key={index} className="mb-4">
          <div className="flex items-center justify-between">
            <span className="text-sm font-semibold text-white">{skill.skill}</span>
            <span className="text-lg text-gray-600 font-bold">{skill.percentage}%</span>
          </div>
          <div className="h-4 mt-2 bg-gray-gradient rounded-full overflow-hidden">
            <div className="h-full bg-blue-gradient" style={{ width: `${skill.percentage}%` }}></div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default SkillBar;