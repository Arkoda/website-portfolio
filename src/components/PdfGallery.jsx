import React from 'react';
import { pdfFiles } from '../constants';
import { ukplaceholder, cvplaceholder, bwdplaceholder, gibbplaceholder } from '../assets';
import JSZip from 'jszip';

const PdfGallery = () => {
  const handleDownloadAll = async () => {
    const zip = new JSZip();
    const folder = zip.folder('pdfs'); // Create a folder in the zip file

    // Add each PDF file to the folder
    pdfFiles.forEach((pdfFile) => {
      folder.file(pdfFile.name, fetch(pdfFile.url).then((response) => response.blob()));
    });

    // Generate the zip file
    const content = await zip.generateAsync({ type: 'blob' });

    // Create a download link for the zip file
    const anchor = document.createElement('a');
    anchor.href = URL.createObjectURL(content);
    anchor.target = '_blank';
    anchor.rel = 'noopener noreferrer';
    anchor.download = 'giuseppe_greco_documents.zip'; // Set the zip file name
    anchor.click();
  };

  return (
    <div
      id="docs"
      className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 p-4 mt-8"
    >
      {/* Title Section */}
      <div className="col-span-3 text-center">
        <h2 className="text-3xl md:text-4xl lg:text-5xl font-semibold text-white text-gradient mb-2">
          Important Documents
        </h2>
        <p className="text-xl md:text-2xl lg:text-xl font-medium text-white text-gradient">
          Browse through essential documents and certifications
        </p>
      </div>

      {/* PDF Files */}
      <div className="col-span-3 md:col-span-2 lg:col-span-3">
        <div className="flex flex-wrap -mx-4">
          {pdfFiles.map((pdfFile, index) => (
            <div key={index} className="w-full sm:w-1/2 lg:w-1/3 px-4 mb-4">
              <a
                href={pdfFile.url}
                target="_blank"
                rel="noopener noreferrer"
                className="cursor-pointer block"
              >
                <div className="relative bg-gray-800 rounded-lg overflow-hidden">
                  <img
                    src={
                      pdfFile.type === 'uk'
                        ? ukplaceholder
                        : pdfFile.type === 'resume'
                        ? cvplaceholder
                        : pdfFile.type === 'bwd'
                        ? bwdplaceholder
                        : pdfFile.type === 'gibb'
                        ? gibbplaceholder
                        : ukplaceholder // Default placeholder
                    }
                    alt="PDF Preview"
                    className="w-full h-40 md:h-56 object-cover opacity-75 filter blur-sm"
                  />
                  <div className="absolute inset-0 flex items-center justify-center text-center bg-black bg-opacity-40">
                    <div className="w-full">
                      <p className="text-xl md:text-2xl lg:text-3xl font-bold text-white mb-2 z-10 relative">
                        <span className="text-gradient">{pdfFile.name}</span>
                      </p>
                      <p className="text-lg md:text-xl lg:text-lg font-medium text-white z-10 relative">
                        <span className="text-gradient">Click to open</span>
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          ))}
        </div>
      </div>

      {/* Download All Button */}
      <div className="col-span-3 text-center mt-4">
        <button
          onClick={handleDownloadAll}
          className="bg-blue-500 text-white font-semibold py-2 px-4 rounded-full hover:bg-blue-600 transition duration-300"
        >
          Download All PDFs
        </button>
      </div>
    </div>
  );
};

export default PdfGallery;
